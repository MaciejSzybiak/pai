create table "commentAuthor"
(
    "idCommentAuthor" serial
        constraint commentauthor_pk
            primary key,
    "idUser"          integer not null
        constraint commentauthor_user_iduser_fk
            references "user",
    "idComment"       integer
        constraint commentauthor_comment_idcomment_fk
            references comment
);

alter table "commentAuthor"
    owner to zkabzkibzyhpyd;

create unique index commentauthor_idcommentauthor_uindex
    on "commentAuthor" ("idCommentAuthor");

INSERT INTO public."commentAuthor" ("idCommentAuthor", "idUser", "idComment") VALUES (5, 4, 6);
INSERT INTO public."commentAuthor" ("idCommentAuthor", "idUser", "idComment") VALUES (6, 5, 7);
INSERT INTO public."commentAuthor" ("idCommentAuthor", "idUser", "idComment") VALUES (7, 5, 8);