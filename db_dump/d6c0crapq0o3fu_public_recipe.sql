create table recipe
(
    "idRecipe"    serial
        constraint recipe_pk
            primary key,
    content       varchar(2500) not null,
    image         varchar(50)   not null,
    name          varchar(50)   not null,
    "cookingTime" integer       not null
);

alter table recipe
    owner to zkabzkibzyhpyd;

create unique index recipe_idrecipe_uindex
    on recipe ("idRecipe");

INSERT INTO public.recipe ("idRecipe", content, image, name, "cookingTime") VALUES (30, 'Place chicken breasts between 2 layers of plastic wrap and pound to about 1/2-inch thick.
Season both sides of chicken breasts with cayenne, salt, and black pepper; dredge lightly in flour and shake off any excess.
Heat olive oil in a skillet over medium-high heat. Place chicken in the pan, reduce heat to medium, and cook until browned and cooked through, about 5 minutes per side; remove to a plate.
Cook capers in reserved oil, smashing them lightly to release brine, until warmed though, about 30 seconds.
Pour white wine into skillet. Scrape any browned bits from the bottom of the pan with a wooden spoon. Cook until reduced by half, about 2 minutes.
Stir lemon juice, water, and butter into the reduced wine mixture; cook and stir continuously to form a thick sauce, about 2 minutes. Reduce heat to low and stir parsley through the sauce.
Return chicken breasts to the pan cook until heated through, 1 to 2 minutes. Serve with sauce spooned over the top.', 'bb33d6e016600e6c.png', 'Chicken Piccata', 25);
INSERT INTO public.recipe ("idRecipe", content, image, name, "cookingTime") VALUES (31, 'Preheat the oven to 400 degrees F (200 degrees C).
Season chicken thighs on all sides with paprika, salt, and pepper.
Cook bacon in a cast iron skillet or oven-safe pan over medium-high heat until browned, 4 to 5 minutes. Remove from skillet and drain on a paper towel-lined plate. Drain and discard excess grease from skillet.
Return skillet to medium heat and cook chicken thighs, skin-side down, for 3 to 4 minutes. Flip chicken over and place skillet in the preheated oven.
Bake until chicken thighs are no longer pink at the bone and juices run clear, about 30 minutes. An instant-read thermometer inserted near the bone should read 165 degrees F (74 degrees C). Remove chicken to a plate and cover with foil to keep warm. Remove all but 2 tablespoons drippings from skillet.
Return skillet to the stove over medium-high heat. Pour in chicken broth while whisking up brown bits from the bottom of the skillet. Add mushrooms and cook until soft, about 3 to 4 minutes. Pour in heavy whipping cream and whisk together until lightly simmering, then reduce heat to medium-low. Season with salt and pepper, if necessary.
Return chicken and any juices back into skillet; top with bacon and green onions. Serve immediately, spooning sauce over the chicken.', '94415bf96d4aaf51.png', 'Smothered chicken', 60);
INSERT INTO public.recipe ("idRecipe", content, image, name, "cookingTime") VALUES (32, 'Preheat oven to 425 degrees F (220 degrees C.)
In a saucepan, combine chicken, carrots, peas, and celery. Add water to cover and boil for 15 minutes. Remove from heat, drain and set aside.
In the saucepan over medium heat, cook onions in butter until soft and translucent. Stir in flour, salt, pepper, and celery seed. Slowly stir in chicken broth and milk. Simmer over medium-low heat until thick. Remove from heat and set aside.
Place the chicken mixture in bottom pie crust. Pour hot liquid mixture over. Cover with top crust, seal edges, and cut away excess dough. Make several small slits in the top to allow steam to escape.
Bake in the preheated oven for 30 to 35 minutes, or until pastry is golden brown and filling is bubbly. Cool for 10 minutes before serving.', 'c4aa3dd5ea3d27e0.png', 'Chicken Pot Pie', 70);
INSERT INTO public.recipe ("idRecipe", content, image, name, "cookingTime") VALUES (33, 'Put the chicken, carrots, celery and onion in a large soup pot and cover with cold water. Heat and simmer, uncovered, until the chicken meat falls off of the bones (skim off foam every so often).
Take everything out of the pot. Strain the broth. Pick the meat off of the bones and chop the carrots, celery and onion. Season the broth with salt, pepper and chicken bouillon to taste, if desired. Return the chicken, carrots, celery and onion to the pot, stir together, and serve.', '1bb87a36cf5840dd.png', 'Chicken soup', 150);
INSERT INTO public.recipe ("idRecipe", content, image, name, "cookingTime") VALUES (34, 'Rub the chicken breasts with the seasoning salt, cover and refrigerate for 30 minutes.
Preheat oven to 350 degrees F (175 degrees C). Place bacon in a large, deep skillet. Cook over medium high heat until crisp. Set aside.
In a medium bowl, combine the mustard, honey, corn syrup, mayonnaise and dried onion flakes. Remove half of sauce, cover and refrigerate to serve later.
Heat oil in a large skillet over medium heat. Place the breasts in the skillet and saute for 3 to 5 minutes per side, or until browned. Remove from skillet and place the breasts into a 9x13 inch baking dish. Apply the honey mustard sauce to each breast, then layer each breast with mushrooms and bacon. Sprinkle top with shredded cheese.
Bake in preheated oven for 15 minutes, or until cheese is melted and chicken juices run clear. Garnish with parsley and serve with the reserved honey mustard sauce.', '0d27cc7c43db77ce.png', 'Aussie Chicken', 80);