create table "recipeComment"
(
    "idRecipeComment" serial
        constraint recipecomment_pk
            primary key,
    "idComment"       integer not null
        constraint recipecomment_comment_idcomment_fk
            references comment,
    "idRecipe"        integer not null
        constraint recipecomment_recipe_idrecipe_fk
            references recipe
);

alter table "recipeComment"
    owner to zkabzkibzyhpyd;

create unique index recipecomment_idrecipecomment_uindex
    on "recipeComment" ("idRecipeComment");

INSERT INTO public."recipeComment" ("idRecipeComment", "idComment", "idRecipe") VALUES (5, 6, 30);
INSERT INTO public."recipeComment" ("idRecipeComment", "idComment", "idRecipe") VALUES (6, 7, 30);
INSERT INTO public."recipeComment" ("idRecipeComment", "idComment", "idRecipe") VALUES (7, 8, 32);