create table "recipeAuthor"
(
    "idRecipeAuthor" serial
        constraint recipeauthor_pk
            primary key,
    "idUser"         integer not null
        constraint recipeauthor_user_iduser_fk
            references "user",
    "idRecipe"       integer not null
        constraint recipeauthor_recipe_idrecipe_fk
            references recipe
);

alter table "recipeAuthor"
    owner to zkabzkibzyhpyd;

create unique index recipeauthor_idrecipeauthor_uindex
    on "recipeAuthor" ("idRecipeAuthor");

INSERT INTO public."recipeAuthor" ("idRecipeAuthor", "idUser", "idRecipe") VALUES (29, 4, 30);
INSERT INTO public."recipeAuthor" ("idRecipeAuthor", "idUser", "idRecipe") VALUES (30, 4, 31);
INSERT INTO public."recipeAuthor" ("idRecipeAuthor", "idUser", "idRecipe") VALUES (31, 4, 32);
INSERT INTO public."recipeAuthor" ("idRecipeAuthor", "idUser", "idRecipe") VALUES (32, 4, 33);
INSERT INTO public."recipeAuthor" ("idRecipeAuthor", "idUser", "idRecipe") VALUES (33, 5, 34);