create table "recipeSaves"
(
    "idRecipeSaves" serial
        constraint recipesaves_pk
            primary key,
    "idUser"        integer not null
        constraint recipesaves_user_iduser_fk
            references "user",
    "idRecipe"      integer not null
        constraint recipesaves_recipe_idrecipe_fk
            references recipe
);

alter table "recipeSaves"
    owner to zkabzkibzyhpyd;

create unique index recipesaves_idrecipesaves_uindex
    on "recipeSaves" ("idRecipeSaves");

INSERT INTO public."recipeSaves" ("idRecipeSaves", "idUser", "idRecipe") VALUES (5, 5, 31);
INSERT INTO public."recipeSaves" ("idRecipeSaves", "idUser", "idRecipe") VALUES (6, 5, 30);