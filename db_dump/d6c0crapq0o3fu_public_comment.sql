create table comment
(
    "idComment" serial
        constraint comment_pk
            primary key,
    content     varchar(200) not null
);

alter table comment
    owner to zkabzkibzyhpyd;

create unique index comment_idcomment_uindex
    on comment ("idComment");

INSERT INTO public.comment ("idComment", content) VALUES (6, 'My favourite dish!');
INSERT INTO public.comment ("idComment", content) VALUES (7, 'This is alright...');
INSERT INTO public.comment ("idComment", content) VALUES (8, 'I''m not really a fan of celery');