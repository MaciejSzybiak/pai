create table password
(
    "idPassword"   serial
        constraint password_pk
            primary key,
    "passwordHash" varchar(255) not null
);

alter table password
    owner to zkabzkibzyhpyd;

create unique index password_idpassword_uindex
    on password ("idPassword");

INSERT INTO public.password ("idPassword", "passwordHash") VALUES (18, '$2y$10$UMe96cfHtKMvlujlSR/BjOUl61JS51aCkEiQaNKzsnt/eul2KLRau');
INSERT INTO public.password ("idPassword", "passwordHash") VALUES (19, '$2y$10$yXzF0QF3yqY31H.6slbPxecZ1Cmw0qfT/zR8BHuS62o5DXFqyLjWq');