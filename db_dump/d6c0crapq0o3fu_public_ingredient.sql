create table ingredient
(
    "idIngredient" serial
        constraint ingredient_pk
            primary key,
    name           varchar(50) not null
);

alter table ingredient
    owner to zkabzkibzyhpyd;

create unique index ingredient_idingredient_uindex
    on ingredient ("idIngredient");

create unique index ingredient_name_uindex
    on ingredient (name);

INSERT INTO public.ingredient ("idIngredient", name) VALUES (82, 'chicken');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (83, 'pepper');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (84, 'salt');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (85, 'olive oil');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (86, 'white wine');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (87, 'parsley');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (88, 'butter');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (89, 'capers');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (91, 'paprika');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (93, 'bacon');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (94, 'broth');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (95, 'mushrooms');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (96, 'onion');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (98, 'carrot');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (99, 'peas');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (100, 'celery');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (105, 'milk');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (114, 'mustard');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (115, 'honey');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (118, 'oil');
INSERT INTO public.ingredient ("idIngredient", name) VALUES (119, 'cheese');