create table "recipeIngredient"
(
    "idRecipeIngredient" serial
        constraint recipeingredient_pk
            primary key,
    "idRecipe"           integer not null
        constraint recipeingredient_recipe_idrecipe_fk
            references recipe,
    "idIngredient"       integer not null
        constraint recipeingredient_ingredient_idingredient_fk
            references ingredient
);

alter table "recipeIngredient"
    owner to zkabzkibzyhpyd;

create unique index recipeingredient_idrecipeingredient_uindex
    on "recipeIngredient" ("idRecipeIngredient");

INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (47, 30, 82);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (48, 30, 83);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (49, 30, 84);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (50, 30, 85);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (51, 30, 86);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (52, 30, 87);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (53, 30, 88);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (54, 30, 89);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (55, 31, 82);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (56, 31, 91);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (57, 31, 84);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (58, 31, 93);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (59, 31, 94);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (60, 31, 95);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (61, 31, 96);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (62, 32, 82);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (63, 32, 98);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (64, 32, 99);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (65, 32, 100);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (66, 32, 88);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (67, 32, 84);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (68, 32, 83);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (69, 32, 94);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (70, 32, 105);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (71, 33, 82);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (72, 33, 98);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (73, 33, 100);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (74, 33, 96);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (75, 33, 84);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (76, 34, 82);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (77, 34, 84);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (78, 34, 93);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (79, 34, 114);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (80, 34, 115);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (81, 34, 96);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (82, 34, 95);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (83, 34, 118);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (84, 34, 119);
INSERT INTO public."recipeIngredient" ("idRecipeIngredient", "idRecipe", "idIngredient") VALUES (85, 34, 87);