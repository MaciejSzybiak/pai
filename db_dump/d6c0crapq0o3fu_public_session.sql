create table session
(
    "idSession" serial
        constraint session_pk
            primary key,
    token       varchar(32) not null,
    expire      timestamp   not null,
    "idUser"    integer     not null
        constraint session_user_iduser_fk
            references "user"
);

alter table session
    owner to zkabzkibzyhpyd;

create unique index session_idsession_uindex
    on session ("idSession");

create unique index session_iduser_uindex
    on session ("idUser");

create unique index session_token_uindex
    on session (token);

INSERT INTO public.session ("idSession", token, expire, "idUser") VALUES (48, 'b300a62659e8a2b71e0b58724b840925', '2022-02-03 20:23:10.000000', 5);