create table "user"
(
    "idUser"           serial
        constraint user_pk
            primary key,
    nickname           varchar(50) not null,
    email              varchar(50) not null,
    "registrationDate" date        not null,
    "idPassword"       integer     not null
        constraint user_password_idpassword_fk
            references password,
    age                integer     not null
);

alter table "user"
    owner to zkabzkibzyhpyd;

create unique index user_iduser_uindex
    on "user" ("idUser");

create unique index user_idpassword_uindex
    on "user" ("idPassword");

INSERT INTO public."user" ("idUser", nickname, email, "registrationDate", "idPassword", age) VALUES (4, 'Maciej', 'maciek@123.com', '2022-02-03', 18, 33);
INSERT INTO public."user" ("idUser", nickname, email, "registrationDate", "idPassword", age) VALUES (5, 'Mariusz', 'mariusz@mariusz.com', '2022-02-03', 19, 58);