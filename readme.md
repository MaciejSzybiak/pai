# pai
Projekt na przedmiot Wstęp do projektowania aplikacji internetowych.

Jest to aplikacja internetowa zawierająca bazę przepisów i pozwalająca na ich wyszukiwanie
wg składników.

Funkcjonalność:
* zakładanie kont użytkowników
* logowanie i obsługa sesji użytkownika
* dodawanie przepisów
* wyświetlanie przepisów dodanych przez użytkownika
* zapisywanie przepisów do ulubionych
* wyświetlanie ulubionych przepisów
* wyszukiwanie przepisów po składnikach z użyciem Fetch API

Aplikacja została zbudowana w oparciu o PHP, HTML, CSS, JavaScript, docker oraz
PostgreSQL.

### Diagram bazy danych

![db](diagram-erd.png)