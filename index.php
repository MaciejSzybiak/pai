<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('', 'DefaultController');
Routing::get('register', 'DefaultController');
Routing::get('saved', 'DefaultController');
Routing::get('add', 'DefaultController');
Routing::get('view', 'RecipeController');
Routing::get('user_recipes', 'DefaultController');
Routing::get('save', 'RecipeController');
Routing::get('logout', 'SecurityController');
Routing::get('search', 'DefaultController');
Routing::post('login_user', 'SecurityController');
Routing::post('register_user', 'SecurityController');
Routing::post('add_recipe', 'RecipeController');
Routing::post('comment', 'RecipeController');
Routing::post('searchRecipes', 'RecipeController');
Routing::run($path);