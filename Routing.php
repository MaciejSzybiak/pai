<?php

require_once 'src/controller/DefaultController.php';
require_once 'src/controller/SecurityController.php';
require_once 'src/controller/RecipeController.php';

class Routing
{
    public static $routes;

    public static function get($url, $controller) {
        self::$routes[$url] = $controller;
    }

    public static function post($url, $controller) {
        self::$routes[$url] = $controller;
    }

    public static function run($url) {
        $action = explode("/", $url)[0];

        if(!array_key_exists($action, self::$routes)) {
            die("Incorrect url!");
        }

        //call controller
        $controller = self::$routes[$action];
        $object = new $controller;
        $action = $action ?: 'login';
        $object->$action();
    }
}
