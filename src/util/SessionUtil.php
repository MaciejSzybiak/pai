<?php

require_once __DIR__."/../repository/SessionRepository.php";
require_once __DIR__."/../repository/UserRepository.php";
require_once __DIR__."/../model/Session.php";
require_once __DIR__."/../model/User.php";

class SessionUtil
{
    public static string $session_cookie = 'session_cookie';
    public static int $session_timeout = 1200;

    private $sessionRepository;
    private $userRepository;

    public function __construct()
    {
        $this->sessionRepository = new SessionRepository();
        $this->userRepository = new UserRepository();
    }

    public function getCurrentUser(): ?User
    {
        $cookie = $this->getSessionCookie();
        if(!$cookie) {
            return null;
        }
        $token = $cookie;
        try {
            $session = $this->sessionRepository->getSession($token);
            if($this->isSessionValid($session)) {
                return $this->userRepository->getUserByID($session->getIdUser());
            }
            else {
                return null;
            }
        }
        catch (RepositoryException | PDOException $e) {
            return null;
        }
    }

    public function logOutCurrentUser()
    {
        $cookie = $this->getSessionCookie();
        if($cookie) {
            $this->sessionRepository->removeSession($cookie);
            setcookie(self::$session_cookie, "", time() - 360);
        }
    }

    public function logInUser(User $user) :? bool
    {
        try {
            $token = bin2hex(random_bytes(16));
            $expire = time() + self::$session_timeout;
            $this->sessionRepository->removeSessionByUserID($user->getIdUser());
            $session = new Session($user->getIdUser(), $token, time() + self::$session_timeout);
            $this->sessionRepository->addSession($session);
        }
        catch (Exception $e) {
            return false;
        }
        setcookie(self::$session_cookie, $token, $expire);
        return true;
    }

    private function getSessionCookie(): ?string
    {
        if(!isset($_COOKIE[self::$session_cookie])) {
            return null;
        }
        return $_COOKIE[self::$session_cookie];
    }

    private function isSessionValid(Session $session): ?bool
    {
        $isValid = $session->getTimestamp() > time();
        if(!$isValid) {
            $this->sessionRepository->removeSession($session->getToken());
        }
        return $isValid;
    }
}