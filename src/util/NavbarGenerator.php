<?php

class NavbarGenerator
{
    private string $action;
    private array $action_icons = [
        'search' => 'search-r',
        'saved' => 'bookmark-r',
        'user_recipes' => 'file-r',
        'add' => 'plus-r',
        'logout' => 'backspace-r'
    ];
    private array $action_strings = [
        'search' => 'Find recipe',
        'saved' => 'Saved recipes',
        'user_recipes' => 'My recipes',
        'add' => 'Add recipe',
        'logout' => 'Log out'
    ];

    public function __construct() {
        $path = trim($_SERVER['REQUEST_URI'], '/');
        $path = parse_url($path, PHP_URL_PATH);
        $this->action = explode("/", $path)[0];
    }

    public function generate(): string
    {
        $navbar = $this->getLiClass('search');
        $navbar = $navbar.$this->getLiClass('saved');
        $navbar = $navbar.$this->getLiClass('user_recipes');
        $navbar = $navbar.$this->getLiClass('add');
        $navbar = $navbar.'<li class="spacer"></li>';
        return $navbar.$this->getLiClass('logout');
    }

    private function getLiClass(string $liAction): string
    {
        $str = $this->getLiClassHeader($liAction);
        $str = $str.'<a href="'.$liAction.'">';
        $str = $str.'<img class="navimg" src="public/images/icons/'
            .$this->action_icons[$liAction]
            .'.svg"> '.$this->action_strings[$liAction].'</a></li>';
        return $str;
    }

    private function getLiClassHeader(string $liAction): string
    {
        if($this->action === $liAction) {
            return '<li class="navbutton-selected">';
        } else {
            return '<li class="navbutton">';
        }
    }
}