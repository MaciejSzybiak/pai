<?php

require_once 'Repository.php';

class SaveRepository extends Repository
{
    public function toggleSave(int $idUser, int $idRecipe) {
        if($this->isSaved($idUser, $idRecipe)) {
            $this->unSave($idUser, $idRecipe);
        } else {
            $this->save($idUser, $idRecipe);
        }
    }

    public function isSaved(int $idUser, int $idRecipe): bool
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public."recipeSaves"
                    WHERE "idRecipe" = :recipe AND "idUser" = :user'
        );
        $stmt->bindParam(':recipe', $idRecipe);
        $stmt->bindParam(':user', $idUser);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        return $data !== false;
    }

    private function unSave(int $idUser, int $idRecipe) {
        $stmt = $this->database->connect()->prepare(
            'DELETE from public."recipeSaves" WHERE  "idRecipe" = :recipe AND "idUser" = :user'
        );
        $stmt->bindParam(':recipe', $idRecipe);
        $stmt->bindParam(':user', $idUser);
        $stmt->execute();
    }

    private function save(int $idUser, int $idRecipe) {
        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public."recipeSaves" ("idUser", "idRecipe") VALUES(?, ?)'
        );
        $stmt->execute([
            $idUser, $idRecipe
        ]);
    }
}