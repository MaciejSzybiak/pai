<?php

require_once 'Repository.php';
require_once __DIR__.'/../model/Password.php';
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/FullRecipe.php';

class RecipeRepository extends Repository
{
    public function addRecipe(Recipe $recipe, User $author, array $ingredients) :? int
    {
        $dbc = $this->database->connect();
        $stmt = $dbc->prepare(
            'INSERT INTO public.recipe (content, image, name, "cookingTime") 
                    VALUES (?, ?, ?, ?)'
        );

        $stmt->execute([
           $recipe->getContent(),
           $recipe->getImage(),
           $recipe->getName(),
           $recipe->getCookingTime()
        ]);

        $recipeId = $dbc->lastInsertId();

        $this->addAuthor($recipeId, $author);
        $this->addIngredients($ingredients, $recipeId);

        return $recipeId;
    }

    /**
     * @throws RepositoryException
     */
    public function getFullRecipe(int $recipeId): ?FullRecipe
    {
        $stmt = $this->database->connect()->prepare(
            'select *
                from "recipe" r, "recipeAuthor" ra, public.user u
                where r."idRecipe" = :id
                and ra."idRecipe" = r."idRecipe"
                and u."idUser" = ra."idUser"'
        );
        $stmt->bindParam(':id', $recipeId);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if(!$data) {
            throw new RepositoryException("Query failed");
        }

        $user = new User($data['idUser'], $data['nickname'], $data['email'], $data['registrationDate'],
            $data['age'], $data['idPassword']);
        return new FullRecipe($data['content'], $data['image'], $data['name'],
                                $data['idRecipe'], $data['cookingTime'], $user,
                                $this->getIngredients($recipeId));
    }

    /**
     * @throws RepositoryException
     */
    public function getUserRecipes(int $idUser): ?array
    {
        $stmt = $this->database->connect()->prepare(
            'select *
                    from "recipe" r, "recipeAuthor" ra, public.user u
                    where ra."idRecipe" = r."idRecipe"
                    and ra."idUser" = u."idUser"
                    and u."idUser" = :id'
        );
        $stmt->bindParam(':id', $idUser);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $recipes = [];
        foreach ($datas as $data) {
            $user = new User($data['idUser'], $data['nickname'], $data['email'], $data['registrationDate'],
                $data['age'], $data['idPassword']);
            $recipes[] = new FullRecipe($data['content'], $data['image'], $data['name'],
                $data['idRecipe'], $data['cookingTime'], $user,
                $this->getIngredients($data['idRecipe']));
        }
        return $recipes;
    }

    public function getSavedRecipes(int $idUser): ?array
    {
        $stmt = $this->database->connect()->prepare(
            'select r.*
                    from "recipe" r, public.user u, public."recipeSaves" rs
                    where rs."idRecipe" = r."idRecipe"
                      and rs."idUser" = u."idUser"
                      and u."idUser" = :id'
        );
        $stmt->bindParam(':id', $idUser);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $recipes = [];
        foreach ($datas as $data) {
            $recipes[] = new FullRecipe($data['content'], $data['image'], $data['name'],
                $data['idRecipe'], $data['cookingTime'], null,
                $this->getIngredients($data['idRecipe']));
        }
        return $recipes;
    }

    public function getRecipesWithIngredients(array $ingredients): ?array
    {
        $q = implode(',', array_fill(0, count($ingredients), '?'));

        $stmt = $this->database->connect()->prepare(
            'SELECT r."idRecipe", r.image, r.name, r."cookingTime", count(*) matching_ingredients
                    FROM ingredient i, "recipeIngredient" ri, recipe r
                    WHERE lower(i.name) IN ('.$q.') AND
                          ri."idIngredient" = i."idIngredient" AND
                          r."idRecipe" = ri."idRecipe"
                    GROUP BY r."idRecipe"
                    ORDER BY r."idRecipe" DESC'
        );
        $stmt->execute($ingredients);

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @throws RepositoryException
     */
    public function getIngredients(int $recipeId): ?array {
        $stmt = $this->database->connect()->prepare(
            'select * from public.ingredient i, public."recipeIngredient" ri
                    where ri."idRecipe" = :id and ri."idIngredient" = i."idIngredient"'
        );
        $stmt->bindParam(':id', $recipeId);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!$data) {
            return [];
        }

        $ingredients = [];
        foreach ($data as $val) {
            $ingredients[] = $val['name'];
        }
        return $ingredients;
    }

    private function addAuthor(int $recipeId, User $author)
    {
        $dbc = $this->database->connect();
        $stmt = $dbc->prepare(
            'INSERT INTO public."recipeAuthor" ("idUser", "idRecipe") VALUES(?, ?)'
        );
        $stmt->execute([$author->getIdUser(), $recipeId]);
    }

    private function addIngredients(array $ingredients, int $idRecipe)
    {
        $dbc = $this->database->connect();
        $stmt = $dbc->prepare(
            'INSERT INTO ingredient (name) VALUES(?) 
                    ON CONFLICT DO NOTHING'
        );
        foreach ($ingredients as $ingredient) {
            $stmt->execute([$ingredient]);
        }
        $this->addRecipeIngredients2($ingredients, $idRecipe);
    }

    private function addRecipeIngredients2(array $ingredients, int $idRecipe)
    {
        $dbc = $this->database->connect();
        $stmt = $dbc->prepare('select * from ingredient where name = :ingredient');
        $ids = [];
        foreach ($ingredients as $i) {
            $stmt->bindParam(':ingredient', $i);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if($data) {
                $ids[] = $data['idIngredient'];
            }
        }

        $stmt = $dbc->prepare(
            'INSERT INTO public."recipeIngredient" ("idRecipe", "idIngredient") VALUES(?, ?)'
        );
        foreach ($ids as $id) {
            $stmt->execute([$idRecipe, $id]);
        }
    }
}