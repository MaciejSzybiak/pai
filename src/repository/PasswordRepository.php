<?php

require_once 'Repository.php';
require_once __DIR__.'/../model/Password.php';
require_once __DIR__.'/../model/User.php';

class PasswordRepository extends Repository
{
    /**
     * @throws RepositoryException
     */
    public function getPassword(User $user): ?Password
    {
        $passwordId = $user->getIdPassword();
        return $this->getPasswordByID($passwordId);
    }

    /**
     * @throws RepositoryException
     */
    public function getPasswordByID(int $id): ?Password
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.password WHERE "idPassword" = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $pwd = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($pwd == false) {
            throw new RepositoryException("Query failed");
        }

        return new Password(
            $pwd['passwordHash']
        );
    }

    /**
     * @throws RepositoryException
     */
    public function addPassword(string $passwordHash): ?int
    {
        $db = $this->database->connect();
        $stmt = $db->prepare(
            'INSERT INTO password ("passwordHash") VALUES(?)'
        );

        $stmt->execute([$passwordHash]);

        $id = $db->lastInsertId();

        if(!$id) {
            throw new RepositoryException("Query failed");
        }
        return $id;
    }

    public function removePassword(int $id)
    {
        $stmt = $this->database->connect()->prepare('DELETE FROM password WHERE "idPassword" = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }
}