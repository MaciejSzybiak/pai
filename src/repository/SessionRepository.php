<?php

require_once 'Repository.php';
require_once __DIR__.'/../model/Session.php';

class SessionRepository extends Repository
{
    /**
     * @throws RepositoryException
     */
    public function getSession(string $token): ?Session
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.session WHERE "token" = :t');
        $stmt->bindParam(':t', $token);
        $stmt->execute();

        $session = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($session == false) {
            throw new RepositoryException("Query failed");
        }

        return new Session(
            $session['idUser'],
            $session['token'],
            strtotime($session['expire'])
        );
    }

    public function addSession(Session $session)
    {
        $db = $this->database->connect();
        $stmt = $db->prepare(
            'INSERT INTO session (token, expire, "idUser") VALUES(?, ?, ?)'
        );
        $stmt->execute([
            $session->getToken(),
            date('Y-m-d H:i:s', $session->getTimestamp()),
            $session->getIdUser()
        ]);
    }

    public function removeSession(string $token)
    {
        $stmt = $this->database->connect()->prepare(
            'DELETE FROM session WHERE "token" = :t'
        );
        $stmt->bindParam(':t', $token);
        $stmt->execute();
    }

    public function removeSessionByUserID(int $id)
    {
        $stmt = $this->database->connect()->prepare(
            'DELETE FROM session WHERE "idUser" = :id'
        );
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }
}