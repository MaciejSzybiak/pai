<?php

require_once 'Repository.php';
require_once __DIR__.'/../model/User.php';

class UserRepository extends Repository
{
    /**
     * @throws RepositoryException
     */
    public function getUser(string $nickname): ?User
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.user WHERE nickname = :nickname');
        $stmt->bindParam(':nickname', $nickname);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            throw new RepositoryException("Query failed");
        }

        return new User(
            $user['idUser'],
            $user['nickname'],
            $user['email'],
            $user['registrationDate'],
            $user['age'],
            $user['idPassword']
        );
    }

    /**
     * @throws RepositoryException
     */
    public function getUserByID(int $id): ?User
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.user WHERE "idUser" = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            throw new RepositoryException("Query failed");
        }

        return new User(
            $user['idUser'],
            $user['nickname'],
            $user['email'],
            $user['registrationDate'],
            $user['age'],
            $user['idPassword']
        );
    }

    public function addUser(User $user)
    {
        $stmt = $this->database->connect()->prepare(
          'INSERT INTO public.user (nickname, email, "registrationDate", "idPassword", age)
                VALUES (?, ?, ?, ?, ?)'
        );

        $stmt->execute([
            $user->getNickname(),
            $user->getEmail(),
            $user->getRegistrationDate(),
            $user->getIdPassword(),
            $user->getAge()
        ]);
    }
}