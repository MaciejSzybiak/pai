<?php

require_once "Repository.php";
require_once __DIR__.'/../model/Comment.php';

class CommentRepository extends Repository
{
    public function getRecipeComments(int $idRecipe): ?array
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT c.content as content, u.nickname as nickname, c."idComment" as idComment
                    FROM public.user u, public.comment c, public."recipeComment" rc, public."commentAuthor" ra
                    WHERE c."idComment" = rc."idComment" AND
                            rc."idRecipe" = :id AND
                            c."idComment" = ra."idComment" AND
                            u."idUser" = ra."idUser"'
        );
        $stmt->execute([$idRecipe]);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!$data) {
            return [];
        }

        $comments = [];
        foreach ($data as $item) {
            $comments[] = new Comment($item['content'], $item['idcomment'], $item['nickname']);
        }

        return $comments;
    }

    public function addRecipeComment(Comment $comment, int $idAuthor, int $idRecipe)
    {
        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.comment (content) VALUES(?)'
        );
        $stmt->execute([$comment->getContent()]);
        $id = $this->database->connect()->lastInsertId();

        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public."commentAuthor" ("idUser", "idComment") VALUES(?, ?)'
        );
        $stmt->execute([$idAuthor, $id]);

        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public."recipeComment" ("idComment", "idRecipe") VALUES (?, ?)'
        );
        $stmt->execute([$id, $idRecipe]);
    }
}