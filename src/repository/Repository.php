<?php

require_once 'DatabaseWrapper.php';
require_once 'RepositoryException.php';

class Repository
{
    protected $database;

    public function __construct()
    {
        $this->database = DatabaseWrapper::getInstance();
    }
}