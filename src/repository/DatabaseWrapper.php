<?php

require_once __DIR__.'/../../Database.php';

class DatabaseWrapper
{
    private static DatabaseWrapper $instance;
    private $databaseConnection;
    private Database $database;

    private function __construct() {
        $this->database = new Database();
        $this->databaseConnection = $this->database->connect();
    }
    private function __clone() { }

    public static function getInstance(): DatabaseWrapper
    {
        if(!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function connect()
    {
        return $this->databaseConnection;
    }
}