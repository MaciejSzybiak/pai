<?php

require_once 'AppController.php';
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/PasswordRepository.php';
require_once __DIR__.'/../util/SessionUtil.php';

class SecurityController extends AppController
{
    private $userRepository;
    private $passwordRepository;
    private $sessionRepository;
    private $sessionUtil;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->passwordRepository = new PasswordRepository();
        $this->sessionRepository = new SessionRepository();
        $this->sessionUtil = new SessionUtil();
    }

    public function login_user()
    {
        if (!$this->isPost()) {
            return $this->render('login');
        }

        $nickname = $_POST['user'];
        $password = $_POST['password'];

        try {
            $user = $this->userRepository->getUser($nickname);
            $pwd = $this->passwordRepository->getPassword($user);
        }
        catch (RepositoryException | PDOException $e) {
            return $this->render('login', ['message' => 'Invalid username or password']);
        }

        $this->sessionUtil->logOutCurrentUser();

        if ($user->getNickname() !== $nickname ||
            !password_verify($password, $pwd->getPasswordHash())
        ) {
            return $this->render('login', ['message' => 'Invalid username or password']);
        }

        if(!$this->sessionUtil->logInUser($user)) {
            return $this->render('login', ['message' => 'Login failed, please try again']);
        }

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/saved");
    }

    public function register_user()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $nickname = $_POST['user'];
        $email = $_POST['email'];
        $age = $_POST['age'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirm-password'];

        if(!$this->validate_string($nickname) ||
            !$this->validate_string($email) || !filter_var($email, FILTER_VALIDATE_EMAIL) ||
            !$this->validate_string($age) || !filter_var($age, FILTER_VALIDATE_INT) ||
            !$this->validate_string($password) || !$this->validate_string($confirmPassword) ||
            $password !== $confirmPassword
        ) {
            return $this->render('register', ['message' => 'Invalid input data. Please try again']);
        }

        try {
            $passwordId = $this->passwordRepository->addPassword((password_hash($password, PASSWORD_BCRYPT)));
        }
        catch (RepositoryException | PDOException $e) {
            return $this->render('register', ['message' => 'Account creation failed. Please try again'.$e]);
        }
        try {
            $user = new User(0, $nickname, $email, date('Y-m-d'), $age, $passwordId);
            $this->userRepository->addUser($user);
        }
        catch (Exception | TypeError $e) {
            $this->passwordRepository->removePassword($passwordId);
            return $this->render('register', ['message' => 'Account creation failed. Please try again'.$e]);
        }

        $this->render('login', ['message' => 'Account created']);
    }

    public function logout() {
        $this->sessionUtil->logOutCurrentUser();
        $this->render('login');
    }

    private function validate_string(string $str): ?bool
    {
        return $str && strlen($str) > 0;
    }
}