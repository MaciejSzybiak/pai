<?php

require_once 'AppController.php';
require_once __DIR__.'/../util/SessionUtil.php';
require_once __DIR__.'/../repository/RecipeRepository.php';

class DefaultController extends AppController
{
    private SessionUtil $sessionUtil;
    private RecipeRepository $recipeRepository;

    public function __construct()
    {
        parent::__construct();
        $this->sessionUtil = new SessionUtil();
        $this->recipeRepository = new RecipeRepository();
    }

    public function login() {
        $this->render('login');
    }

    public function register() {
        $this->render('register');
    }

    public function saved() {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }

        $recipes = $this->recipeRepository->getSavedRecipes($user->getIdUser());

        $recipeParams = [];
        foreach ($recipes as $fullRecipe) {
            $recipeParams[] = [
                'content' => $fullRecipe->getContent(),
                'image' => 'public/uploads/'.$fullRecipe->getImage(),
                'name' => $fullRecipe->getName(),
                'cookingTime' => $fullRecipe->getCookingTime(),
                'ingredients' => $fullRecipe->getIngredients(),
                'recipeId' => $fullRecipe->getIdRecipe()
            ];
        }

        $params = [
            'recipes' => $recipeParams
        ];

        $this->render('saved', $params);
    }

    public function add() {
        $this->render('add');
    }

    public function user_recipes() {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }

        $recipes = $this->recipeRepository->getUserRecipes($user->getIdUser());

        $recipeParams = [];
        foreach ($recipes as $fullRecipe) {
            $recipeParams[] = [
                'content' => $fullRecipe->getContent(),
                'image' => 'public/uploads/'.$fullRecipe->getImage(),
                'name' => $fullRecipe->getName(),
                'cookingTime' => $fullRecipe->getCookingTime(),
                'ingredients' => $fullRecipe->getIngredients(),
                'recipeId' => $fullRecipe->getIdRecipe()
            ];
        }

        $params = [
            'recipes' => $recipeParams
        ];

        $this->render('user_recipes', $params);
    }

    public function search()
    {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }

        $this->render('search');
    }
}