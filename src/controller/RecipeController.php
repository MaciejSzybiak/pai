<?php

require_once __DIR__.'/../model/Recipe.php';
require_once __DIR__.'/../model/FullRecipe.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/RecipeRepository.php';
require_once __DIR__.'/../repository/SaveRepository.php';
require_once __DIR__.'/../repository/CommentRepository.php';
require_once __DIR__.'/../util/SessionUtil.php';

require_once 'AppController.php';
class RecipeController extends AppController
{
    const MAX_FILE_SIZE = 1024*4086;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private SessionUtil $sessionUtil;
    private RecipeRepository $recipeRepository;
    private SaveRepository  $saveRepository;
    private CommentRepository $commentRepository;
    private string $message;

    public function __construct()
    {
        parent::__construct();
        $this->sessionUtil = new SessionUtil();
        $this->recipeRepository = new RecipeRepository();
        $this->saveRepository = new SaveRepository();
        $this->commentRepository = new CommentRepository();
    }

    public function add_recipe()
    {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }
        if(!$this->validateForm()) {
            return $this->render('add', ['message' => $this->message]);
        }
        if (!$this->isPost()) {
            return $this->render('add');
        }

        $filename = $this->getRandomFilename();
        move_uploaded_file($_FILES['file']['tmp_name'],
            dirname(__DIR__).self::UPLOAD_DIRECTORY.$filename);

        $recipe = new Recipe($_POST['recipe'], $filename, $_POST['name'], -1, $_POST['time']);
        $ingredients = $this->parseIngredients();
        $idRecipe = $this->recipeRepository->addRecipe($recipe, $user, $ingredients);

        $this->renderRecipe($idRecipe, $user->getIdUser());
    }

    public function view() {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }

        if(isset($_GET['id'])) {
            $this->renderRecipe($_GET['id'], $user->getIdUser());
        } else {
            die("Unknown recipe!");
        }
    }

    public function save()
    {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }
        if(isset($_GET['id'])) {
            $this->toggleSave($user, $_GET['id']);
            $this->renderRecipe($_GET['id'], $user->getIdUser());
        } else {
            die("Unknown recipe!");
        }
    }

    public function comment()
    {
        $user = $this->sessionUtil->getCurrentUser();
        if(!$user) {
            return $this->render('login', ['message' => 'Please login to continue']);
        }
        if(!$this->isPost()) {
            return $this->render('login');
        }
        if(!$_POST['comment'] || strlen($_POST['comment']) <= 0) {
            if($_GET['id']) {
                return $this->renderRecipe($_GET['id'], $user->getIdUser(),
                                    "Comment cannot be empty!");
            }
        }

        if($_GET['id']) {
            $this->commentRepository->addRecipeComment(
                new Comment($_POST['comment'], 0, $user->getNickname()),
                $user->getIdUser(), $_GET['id']
            );
            $this->renderRecipe($_GET['id'], $user->getIdUser());
        } else {
            die("Unknown recipe");
        }
    }

    public function searchRecipes()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        $user = $this->sessionUtil->getCurrentUser();
        if($user) {
            if ($contentType === "application/json") {
                $content = trim(file_get_contents("php://input"));
                $decoded = json_decode($content, true);

                header('Content-type: application/json');
                http_response_code(200);

                try {
                    $recipes = $this->recipeRepository->getRecipesWithIngredients($decoded);
                    foreach ($recipes as &$recipe) {
                        try {
                            $recipe += ['ingredients' => $this->recipeRepository->getIngredients($recipe['idRecipe'])];
                        } catch (RepositoryException | PDOException $e)
                        {
                            $recipe += ['ingredients' => []];
                        }
                    }
                    echo json_encode($recipes);
                } catch (RepositoryException | PDOException $e) {
                    echo json_encode([]);
                }
            }
        }
    }

    private function toggleSave(User $user, int $recipeId) {
        $this->saveRepository->toggleSave($user->getIdUser(), $recipeId);
    }

    private function renderRecipe(int $recipeId, int $idUser, ?string $message = null) {
        try {
            $fullRecipe = $this->recipeRepository->getFullRecipe($recipeId);
            $recipeComments = $this->commentRepository->getRecipeComments($recipeId);
        } catch (RepositoryException | PDOException $e) {
            die("Unknown recipe");
        }
        $comments = [];
        foreach ($recipeComments as $recipeComment) {
            $comments[] = [
                'content' => $recipeComment->getContent(),
                'author' => $recipeComment->getAuthor()
            ];
        }
        $this->render('view',
            [
                'content' => $fullRecipe->getContent(),
                'image' => 'public/uploads/'.$fullRecipe->getImage(),
                'name' => $fullRecipe->getName(),
                'cookingTime' => $fullRecipe->getCookingTime(),
                'ingredients' => $fullRecipe->getIngredients(),
                'idRecipe' => $recipeId,
                'isSaved' => $this->saveRepository->isSaved($idUser, $recipeId),
                'comments' => $comments,
                'message' => $message
            ]);
    }

    private function validateForm(): ?bool
    {
        $file = $_FILES['file'];
        if(!is_uploaded_file($file['tmp_name'])) {
            $this->message = 'Please upload a picture';
            return false;
        }
        if($file['size'] > self::MAX_FILE_SIZE) {
            $this->message = 'Picture is too large';
            return false;
        }
        if(!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message = 'Picture type is not supported';
        }
        if(!$this->validate_string($_POST['name']) ||
            !$this->validate_string($_POST['time']) ||
            !$this->validate_string($_POST['ingredients']) ||
            !$this->validate_string($_POST['recipe'])
        ) {
            $this->message = 'Please fill the entire form';
            return false;
        }
        return true;
    }

    private function validate_string(string $str): ?bool
    {
        return $str && strlen($str) > 0;
    }

    private function getRandomFilename(): ?string
    {
        if($_FILES['file']['type'] === 'image/png') {
            $extension = '.png';
        } else {
            $extension = '.jpg';
        }
        try {
            return bin2hex(random_bytes(8)).$extension;
        } catch (Exception $e) {
            return 'failed'.$extension;
        }
    }

    private function parseIngredients(): ?array
    {
        $chars = array("\n", "\r", "\t", "\v", "\x00");
        $array = explode(",", $_POST['ingredients']);
        foreach ($array as &$val) {
            $val = str_replace($chars, " ", trim($val));
        }
        return $array;
    }
}