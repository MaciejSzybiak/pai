<?php

class Session
{
    private int $idUser;
    private string $token;
    private int $timestamp;

    /**
     * @param int $idUser
     * @param string $token
     * @param int $timestamp
     */
    public function __construct(int $idUser, string $token, int $timestamp)
    {
        $this->idUser = $idUser;
        $this->token = $token;
        $this->timestamp = $timestamp;
    }

    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }


}