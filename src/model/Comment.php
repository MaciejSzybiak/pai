<?php

class Comment
{
    private string $content;
    private int $idComment;
    private string $author;

    /**
     * @param $content
     * @param int $idComment
     * @param string $author
     */
    public function __construct($content, int $idComment, string $author)
    {
        $this->content = $content;
        $this->idComment = $idComment;
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getIdComment(): int
    {
        return $this->idComment;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }
}
