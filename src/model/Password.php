<?php

class Password
{
    private $passwordHash;

    /**
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->passwordHash = $password;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
}
