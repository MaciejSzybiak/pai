<?php

class User
{
    private $idUser;
    private $nickname;
    private $email;
    private $registrationDate;
    private $age;
    private $idPassword;

    /**
     * @param string $nickname
     * @param string $email
     * @param string $registrationDate
     * @param int $age
     * @param int $idPassword
     */
    public function __construct(int $idUser, string $nickname, string $email, string $registrationDate, int $age, int $idPassword)
    {
        $this->idUser = $idUser;
        $this->nickname = $nickname;
        $this->email = $email;
        $this->registrationDate = $registrationDate;
        $this->age = $age;
        $this->idPassword = $idPassword;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getRegistrationDate(): string
    {
        return $this->registrationDate;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return int
     */
    public function getIdPassword(): int
    {
        return $this->idPassword;
    }

    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
}
