<?php

class Recipe
{
    private int $idRecipe;
    private $content;
    private $image;
    private $name;
    private int $cookingTime;

    /**
     * @param string $content
     * @param string $image
     * @param string $name
     * @param int $idRecipe
     * @param int $cookingTime
     */
    public function __construct(string $content, string $image, string $name, int $idRecipe, int $cookingTime)
    {
        $this->content = $content;
        $this->image = $image;
        $this->name = $name;
        $this->idRecipe = $idRecipe;
        $this->cookingTime = $cookingTime;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getIdRecipe(): int
    {
        return $this->idRecipe;
    }

    /**
     * @return int
     */
    public function getCookingTime(): int
    {
        return $this->cookingTime;
    }
}
