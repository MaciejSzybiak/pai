<?php

require_once "Recipe.php";
require_once "User.php";
require_once "Ingredient.php";

class FullRecipe extends Recipe
{
    private ?User $author;
    private array $ingredients;

    /**
     * @param ?User $author
     * @param array $ingredients
     */
    public function __construct(string $content, string $image, string $name,
                                int $idRecipe, int $cookingTime,
                                ?User $author, array $ingredients)
    {
        parent::__construct($content, $image, $name, $idRecipe, $cookingTime);
        $this->author = $author;
        $this->ingredients = $ingredients;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }
}