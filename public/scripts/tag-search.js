let inputField = document.getElementById("ingredient-input")
let listParent = document.getElementById("ingredient-list")
let recipeParent = document.getElementById("recipe-list")

inputField.addEventListener("keyup", function (event) {
    let text = this.value.trim()

    if (text.endsWith(",")) {
        let replaced = text.replace(",", "")
        if(replaced.length > 1) {
            createElement(replaced)
            getRecipes()
        }
        this.value = ""
    }
})

function createElement(name) {
    let li = document.createElement("li");
    li.className = `ingredient-tag`
    li.innerHTML = `${name}&nbsp;<a>&times;</a>`
    li.setAttribute("id", `ing-${name}`)
    li.setAttribute("ingredient-name", `${name}`)
    li.querySelector("a").addEventListener("click", function () {
        onDelete(name)
    })
    listParent.appendChild(li)
}

function onDelete(name) {
    let li = document.getElementById(`ing-${name}`)
    li.remove()
    getRecipes()
}

function getRecipes() {
    let data = []
    let children = Array.from(listParent.children)
    children.forEach((val, index) => {
        data.push(val.getAttribute("ingredient-name"))
    })

    fetch("/searchRecipes", {
        method: "POST",
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.json()
    }).then(function (recipes) {
        recipeParent.innerHTML = ""
        loadRecipes(recipes, data)
    })
}

function loadRecipes(recipes, searchIngredients) {
    recipes.forEach(recipe => {
        createRecipe(recipe, searchIngredients)
    })
}

function createRecipe(recipe, searchIngredients) {
    let template = document.querySelector("#recipe-template")

    let clone = template.content.cloneNode(true);

    let image = clone.querySelector("img")
    image.src = `public/uploads/${recipe.image}`

    let link = clone.querySelector("a")
    link.href = `view?id=${recipe.idRecipe}`
    link.innerHTML = `${recipe.name}`

    let p = clone.querySelector("p")
    p.innerHTML = `${recipe.cookingTime} min.`

    let ingredientList = clone.querySelector(".dish-ingredients")
    recipe.ingredients.forEach(ingredient => {
        let div = document.createElement("div")
        div.innerHTML = `${ingredient}`
        if (!searchIngredients.includes(`${ingredient}`)) {
            div.className = "ingredient-not-present"
        } else {
            div.className = "ingredient-present"
        }
        ingredientList.appendChild(div)
    })

    recipeParent.appendChild(clone)
}