<!DOCTYPE html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>Login</title>
</head>

<body>
    <div class="container">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="login-container">
            <form class="login" action="login_user" method="POST">
                <input name="user" type="text" placeholder="User">
                <input name="password" type="password" placeholder="Password">
                <button type="submit">Log in</button>
            </form>
            <a href="/register">Create an account</a>
            <div class="error-text">
                <?php if(isset($message)) {
                    echo $message;
                }
                ?>
            </div>
        </div>
    </div>
</body>
