<!DOCTYPE html>

<?php
require_once __DIR__.'/../../src/util/NavbarGenerator.php'
?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/website.css">
    <link rel="stylesheet" type="text/css" href="public/css/basic-content.css">
    <link rel="stylesheet" type="text/css" href="public/css/search.css">
    <script src="public/scripts/tag-search.js" defer></script>
    <title>Find recipe</title>
</head>


<body>
<div class="content-wrapper">
    <div class="website-content">
        <div>
            <ul id="ingredient-list" class="ingredient-list"></ul>
            <form action="find-ingredient" method="POST" class="ingredient-search" onsubmit="return false;">
                <input type="text" placeholder="Find ingredient" id="ingredient-input">
            </form>
        </div>
        <div id="recipe-list"></div>
    </div>
    <div class="sidenav">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="sidenav-menu">
            <nav>
                <ul>
                    <?php
                    $navbarGenerator = new NavbarGenerator();
                    echo $navbarGenerator->generate();
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>

<template id="recipe-template">
    <div class="dish-templated">
        <div class="dish-img">
            <img src="">
        </div>
        <div class="dish-header">
            <div class="dish-header-name">
                <a href="view?id=">recipe_name</a>
            </div>
            <div class="dish-header-time">
                <img src="public/images/icons/clock-r.svg">
                <p>cooking_time min.</p>
            </div>
        </div>
        <div class="dish-ingredients"></div>
    </div>
</template>
