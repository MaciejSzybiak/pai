<!DOCTYPE html>

<?php
require_once __DIR__.'/../../src/util/NavbarGenerator.php'
?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/website.css">
    <link rel="stylesheet" type="text/css" href="public/css/basic-content.css">
    <title><?php
        if(isset($name)) {
            echo $name;
        }
        ?> - Recipe</title>
</head>

<body>
<div class="content-wrapper">
    <div class="website-content">
        <div>
            <div class="basic-header">
                <?php
                if(isset($name)) {
                    echo $name;
                }
                if(isset($isSaved) && isset($idRecipe)) {
                    if ($isSaved) {
                        echo '<a href="save?id=' . $idRecipe . '"><img src="public/images/icons/bookmark-green.svg"></a>';
                    } else {
                        echo '<a href="save?id=' . $idRecipe . '"><img src="public/images/icons/bookmark-r.svg"></a>';
                    }
                } else {
                    echo '<a href="#"><img src="public/images/icons/bookmark-r.svg"></a>';
                }
                ?>
            </div>
            <div class="view-panel">
                <div class="view-left">
                    <div class="ingredient-list">
                        <?php
                        if(isset($ingredients)) {
                            foreach ($ingredients as $ingredient) {
                                echo '<div>'.$ingredient.'</div>';
                            }
                        }
                        ?>
                    </div>
                    <div class="recipe-body">
                        <?php
                        if(isset($content)) {
                            echo $content;
                        }
                        ?>
                    </div>
                </div>
                <div class="view-right">
                    <div class="view-image">
                        <img src="<?php
                        if(isset($image)) {
                            echo $image;
                        }
                        ?>">
                    </div>
                    <div class="comments-container">
                        Comments
                        <form action="comment<?php if(isset($idRecipe)) echo '?id='.$idRecipe; ?>" method="POST" class="comment-form">
                            <textarea name="comment" placeholder="Add your comment..."></textarea>
                            <div class="input-twocolumn">
                                <div class="error-text">
                                    <?php if(isset($message)) {
                                        echo $message;
                                    }
                                    ?>
                                </div>
                                <button type="submit" class="post-button">Post</button>
                            </div>
                        </form>
                        <?php
                        if(isset($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                        <div class="comment-box">
                            <div class="comment-box-header">
                                <?php echo $comment['author'];?>
                            </div>
                            <div class="comment-box-content">
                                <?php echo $comment['content'];?>
                            </div>
                        </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidenav">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="sidenav-menu">
            <nav>
                <ul>
                    <?php
                    $navbarGenerator = new NavbarGenerator();
                    echo $navbarGenerator->generate();
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
