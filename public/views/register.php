<!DOCTYPE html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>Register</title>
</head>

<body>
    <div class="container">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="register-container">
            <form class="register" action="register_user" method="POST">
                <input name="user" type="text" placeholder="User">
                <input name="email" type="text" placeholder="Email">
                <input name="age" type="text" placeholder="Age">
                <input name="password" type="password" placeholder="Password">
                <input name="confirm-password" type="password" placeholder="Confirm password">
                <button type="submit">Register</button>
            </form>
            <div class="error-text">
                <?php if(isset($message)) {
                    echo $message;
                }
                ?>
            </div>
        </div>
    </div>
</body>
