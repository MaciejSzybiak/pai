<!DOCTYPE html>

<?php
require_once __DIR__.'/../../src/util/NavbarGenerator.php'
?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/website.css">
    <link rel="stylesheet" type="text/css" href="public/css/basic-content.css">
    <title>Add recipe</title>
</head>

<body>
<div class="content-wrapper">
    <div class="website-content">
        <div>
            <div class="basic-header">Add recipe</div>
            <form action="add_recipe" method="POST" ENCTYPE="multipart/form-data">
                <input name="name" type="text" placeholder="Dish name">
                <input name="time" type="number" placeholder="Cooking time (minutes)", min="1">
                <input name="ingredients" type="text" placeholder="Ingredients (separated by comma)">
                <input name="file" type="file" placeholder="Add picture">
                <textarea name="recipe" placeholder="Cooking instructions"></textarea>
                <div class="error-text">
                    <?php if(isset($message)) {
                        echo $message;
                    }
                    ?>
                </div>
                <button type="submit" class="post-button">Post</button>
            </form>
        </div>
    </div>
    <div class="sidenav">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="sidenav-menu">
            <nav>
                <ul>
                    <?php
                    $navbarGenerator = new NavbarGenerator();
                    echo $navbarGenerator->generate();
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
