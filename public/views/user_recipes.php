<!DOCTYPE html>

<?php
require_once __DIR__.'/../../src/util/NavbarGenerator.php'
?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/website.css">
    <link rel="stylesheet" type="text/css" href="public/css/grid-content.css">
    <title>Saved recipes</title>
</head>

<body>
<div class="content-wrapper">
    <div class="website-content">
        <?php
        if(isset($recipes)) {
            foreach ($recipes as $recipe) {
        ?>
        <div>
            <div class="dish-img">
                <img src="<?php echo $recipe['image']; ?>">
            </div>
            <div class="dish-header">
                <div class="dish-header-name">
                    <a href="view?id=<?php echo $recipe['recipeId']?>"><?php echo $recipe['name']; ?></a>
                </div>
                <div class="dish-header-time">
                    <img src="public/images/icons/clock-r.svg">
                    <p><?php echo $recipe['cookingTime']." min."; ?></p>
                </div>
            </div>
            <div class="dish-ingredients">
                <?php
                foreach ($recipe['ingredients'] as $ingredient) {
                    echo '<div>'.$ingredient.'</div>';
                }
                ?>
            </div>
        </div>
        <?php
            }
        }
        ?>
    </div>
    <div class="sidenav">
        <div class="logo">
            <img src="public/images/logo.png">
        </div>
        <div class="sidenav-menu">
            <nav>
                <ul>
                    <?php
                    $navbarGenerator = new NavbarGenerator();
                    echo $navbarGenerator->generate();
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
